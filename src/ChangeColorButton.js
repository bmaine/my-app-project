import React from'react';


class ChangeColorButton extends React.Component {
  constructor() {
    super();
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick () {
    console.log("clicked");
    this.props.clickHandler();
  }
  
  render() {
    return(
      <button onClick = {this.handleClick}>I don't like {this.props.color}</button>
    )}
} 

export default ChangeColorButton;