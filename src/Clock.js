import React from 'react';


class ClockComponent extends React.Component {
    constructor() {
        super();
        this.state = {time : new Date().toLocaleString()};
    }

    componentDidMount() {
        this.intervalId = setInterval(
            () => {return (this.tick())},500)
    }

    componentWillUnmount() {
        clearInterval(this.intervalId);
    };

    tick() {
        this.setState({
            time : new Date().toLocaleString()
        })
    };

    render() {
        return(
            <p>{this.state.time}</p>
        )
    };

};

export default ClockComponent;