import React from'react';
import ChangeColorButton from './ChangeColorButton';

class ColoredBlock extends React.Component {
  constructor() {
    super();
    this.state = {
      color : "red"
    }
    
    this.changeColor = this.changeColor.bind(this);
  }

  changeColor() {
    let newColor = this.state.color === "red" ? "blue" : "red";
    this.setState({
        color : newColor
    })
    console.log(this.state.color);
    
  }

  render() {
    return(
      <div style = {{width: "200px", height: "200px", backgroundColor: this.state.color}}><ChangeColorButton clickHandler = {this.changeColor} color = {this.state.color}></ChangeColorButton></div>
    )
  }
} 


export default ColoredBlock;