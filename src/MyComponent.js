import React from 'react';

class MyComponent extends React.Component {
    constructor() {
        super();
        this.state = {
            button : "Click Me"
        };
        this.handleClick = this.handleClick.bind(this);
    };

    handleClick() {
        this.setState({
            button : "Yolo"
        })
    }

        render() {
            return (
                <button onClick = {this.handleClick}>{this.state.button}</button>
            )
        };
    
};

export default MyComponent;