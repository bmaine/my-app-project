import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import ColoredBlock from './ColoredBlock';
import Products from './Products';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome on Benjamin App</h1>
        </header>
        <p className="App-intro"> You can follow my progression <a href="https://gitlab.com/bmaine/my-app-project" alt="my-app-name-progression-link"> here </a></p>
        <Products></Products>
      </div>
      
    );
  }
}

export default App;
